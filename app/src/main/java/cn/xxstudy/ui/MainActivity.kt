package cn.xxstudy.ui

import android.content.res.Resources
import android.graphics.Insets
import android.graphics.Typeface
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowInsets
import androidx.appcompat.app.AppCompatActivity
import cn.xxstudy.ui_library.tab.bottom.UiBottomInfo
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val windowMetrics = windowManager.currentWindowMetrics
        val insets = windowMetrics.windowInsets
            .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
        val height=windowMetrics.bounds.height()-insets.bottom
        val width=windowMetrics.bounds.width()-insets.left-insets.right

        Log.d(
            "MainActivity",
            "onCreate(MainActivity.java:24):${windowMetrics.bounds.height()} height=$height widht=$width"
        )

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        Log.d(
            "MainActivity",
            "onCreate(MainActivity.java:34):  height=${displayMetrics.heightPixels} widht=${displayMetrics.widthPixels}"
        )


        val displayMetrics2 = resources.displayMetrics
        Log.d(
            "MainActivity",
            "onCreate(MainActivity.java:39):  height=${displayMetrics2.heightPixels} widht=${displayMetrics2.widthPixels}"
        )

        val displayMetrics3 = Resources.getSystem().displayMetrics
        Log.d(
            "MainActivity",
            "onCreate(MainActivity.java:44):  height=${displayMetrics3.heightPixels} widht=${displayMetrics3.widthPixels}"
        )


        val typeFace = Typeface.createFromAsset(assets, "fonts/fontello.ttf")
        val bottomInfo = UiBottomInfo(
            title = "主页",
            iconFontTtfName = "fonts/fontello.ttf",
            defaultIcon = resources.getString(R.string.home),
            selectedIcon = resources.getString(R.string.gps)
        )

        tv.setUiTabInfo(bottomInfo)
    }
}