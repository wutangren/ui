package cn.xxstudy.ui_library.tab.common

import android.view.ViewGroup

/**
 * @author ：DELL on 2021/4/2 .
 * @packages ：cn.xxstudy.ui_library.tab.common .
 * TODO:一句话描述
 */
interface IUiTabLayout<Tab : ViewGroup, D> {
    fun findTab(data: D): Tab
    fun addTabSelectedChangeListener(listener: OnTabSelectedChangeListener<D>)
    fun defaultSelected(defaultInfo: D)
    fun inflaterInfo(infoList: List<D>)

    interface OnTabSelectedChangeListener<D> {
        fun onTabSelectedChange(index: Int, prevInfo: D, nextInfo: D)
    }
}