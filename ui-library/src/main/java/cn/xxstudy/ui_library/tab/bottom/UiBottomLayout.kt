package cn.xxstudy.ui_library.tab.bottom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.widget.FrameLayout
import cn.xxstudy.ui_library.tab.common.IUiTabLayout
import cn.xxstudy.ui_library.util.dp2Px

/**
 * @author ：DELL on 2021/4/2 .
 * @packages ：cn.xxstudy.ui_library.tab.bottom .
 * TODO:BottomLayout
 * 1. 透明度
 * 2. 某个Item 突出
 * 3. BottomLayout不遮挡可滑动的view最底部视图
 */
class UiBottomLayout @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attributeSet, defStyle), IUiTabLayout<UiTabBottom, UiBottomInfo> {
    private var mBottomInfoList: List<UiBottomInfo>? = null
    private val mTabSelectedChangeListener =
        ArrayList<IUiTabLayout.OnTabSelectedChangeListener<UiBottomInfo>>()
    var bottomAlpha = 1f
    var tabBottomHeight = 50f.dp2Px
    var bottomTopLineColor = Color.parseColor("#dfe0e1")

    private var mCurrentBottomInfo: UiBottomInfo? = null

    override fun findTab(data: UiBottomInfo): UiTabBottom {
return UiTabBottom(context)
    }

    override fun addTabSelectedChangeListener(listener: IUiTabLayout.OnTabSelectedChangeListener<UiBottomInfo>) {

    }

    override fun defaultSelected(defaultInfo: UiBottomInfo) {

    }

    override fun inflaterInfo(infoList: List<UiBottomInfo>) {
        if (infoList.isEmpty()) {
            return
        }
        this.mBottomInfoList = infoList
        //移除之前已经添加的
        for (index in childCount - 1 downTo 1) {
            removeViewAt(index)
        }
        mCurrentBottomInfo = null
        addBottomBg()
        //移除之前添加的监听
        val iterator = mTabSelectedChangeListener.iterator()
        while (iterator.hasNext()) {
            if (iterator.next() is UiTabBottom) {
                iterator.remove()
            }
        }

        val bottomLayout = FrameLayout(context)

    }

    private fun addBottomBg() {

    }
}