package cn.xxstudy.ui_library.tab.common

/**
 * @author ：DELL on 2021/4/2 .
 * @packages ：cn.xxstudy.ui_library.tab.common .
 * TODO:一句话描述
 */
interface IUiTab<D> : IUiTabLayout.OnTabSelectedChangeListener<D> {
    fun setUiTabInfo(data: D)

    /**
     * 动态修改某个item的大小
     */
    fun resetHeight(height: Int)
}