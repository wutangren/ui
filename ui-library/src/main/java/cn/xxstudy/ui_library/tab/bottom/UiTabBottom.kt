package cn.xxstudy.ui_library.tab.bottom

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import cn.xxstudy.ui_library.R
import cn.xxstudy.ui_library.tab.common.IUiTab
import kotlinx.android.synthetic.main.ui_layout_bottom.view.*
import java.lang.IllegalArgumentException

/**
 * @author ：DELL on 2021/4/2 .
 * @packages ：cn.xxstudy.ui_library.tab.bottom .
 * TODO:TabBottom
 */
class UiTabBottom @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attributeSet, defStyle), IUiTab<UiBottomInfo> {
    private var bottomInfo: UiBottomInfo? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.ui_layout_bottom, this)
    }

    override fun setUiTabInfo(data: UiBottomInfo) {
        this.bottomInfo = data
        inflaterBottom(init = true)
    }

    override fun resetHeight(height: Int) {
        val params = layoutParams
        params.height = height
        layoutParams = params
        bottomTitle.visibility = View.GONE
    }

    override fun onTabSelectedChange(index: Int, prevInfo: UiBottomInfo, nextInfo: UiBottomInfo) {
        if (prevInfo != this.bottomInfo && nextInfo != this.bottomInfo || prevInfo == this.bottomInfo) {
            return
        }
        if (prevInfo == this.bottomInfo) {
            inflaterBottom(false)
        } else {
            inflaterBottom(true)
        }
    }

    /**
     * @param selected 是否选中 默认true
     * @param init 是否是首次加载 默认false
     */
    private fun inflaterBottom(selected: Boolean = true, init: Boolean = false) {
        if (bottomInfo == null) {
            throw IllegalArgumentException("bottomInfo must can't null")
        }

        bottomInfo!!.apply {
            if (tabType == TabType.ICON) {
                if (init) {
                    bottomImg.visibility = View.GONE
                    bottomIcon.visibility = View.VISIBLE
                    bottomIcon.typeface = Typeface.createFromAsset(context.assets, iconFontTtfName)
                    bottomTitle.text = title ?: ""
                }

                bottomIcon.text = if (selected) selectedIcon else defaultIcon
                bottomIcon.setTextColor(if (selected) selectedColor else defaultColor)
                bottomTitle.setTextColor(if (selected) selectedColor else defaultColor)
            } else {
                if (init) {
                    bottomImg.visibility = View.VISIBLE
                    bottomIcon.visibility = View.GONE
                    bottomTitle.visibility = View.GONE
                }
                if (selectedBitmap == null || defaultBitmap == null) {
                    throw IllegalArgumentException("selectedBitmap or defaultBitmap must can't null")
                }
                bottomImg.setImageBitmap(if (selected) selectedBitmap else defaultBitmap)
            }
        }
    }
}