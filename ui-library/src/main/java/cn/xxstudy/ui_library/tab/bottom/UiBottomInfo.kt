package cn.xxstudy.ui_library.tab.bottom

import android.graphics.Bitmap
import android.graphics.Color


enum class TabType {
    BITMAP,
    ICON
}

data class UiBottomInfo(
    val title: String? = null,
    val defaultBitmap: Bitmap? = null,
    val selectedBitmap: Bitmap? = defaultBitmap,
    val iconFontTtfName: String? = null,
    val defaultIcon: String? = null,
    val selectedIcon: String? = defaultIcon,
    val defaultColor: Int = Color.parseColor("#FF6200EE"),
    val selectedColor: Int = Color.parseColor("#FF3700B3"),
    val tabType: TabType = TabType.ICON
)