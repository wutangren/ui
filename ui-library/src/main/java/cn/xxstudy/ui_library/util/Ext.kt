package cn.xxstudy.ui_library.util

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue

/**
 * @author ：DELL on 2021/4/2 .
 * @packages ：cn.xxstudy.ui_library.util .
 * TODO:常用的扩展
 */

val Float.dp2Px: Float
    get() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        Resources.getSystem().displayMetrics
    )

//fun DisplayMetrics.withAndHeight(context: Context): Pair<Float, Float> {
//
//}
//